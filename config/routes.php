<?php

return [
    'task/update/([0-9]+)' => 'task/update/$1',
    'task/create' => 'task/create',

    'site/index/page-([0-9]+)/(\w+)/(desc || asc)' => 'site/index/$1/$2/$3',
    'page-([0-9]+)/(\w+)/(desc || asc)' => 'site/index/$1/$2/$3',

    'site/login' => 'site/login',
    'site/logout' => 'site/logout',

    'index.php' => 'site/index',
    '' => 'site/index', // actionIndex в SiteController
];
