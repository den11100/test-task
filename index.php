<?php

// FRONT CONTROLLER

define('ROOT', dirname(__FILE__));

// errors
ini_set('display_errors',1);
error_reporting(E_ALL);

// log
//ini_set('display_errors',0);
//ini_set('log_errors', 'On');
//ini_set('error_log', ROOT.'/var/log/php_errors.log');

session_start();

// Autoload
require_once(ROOT.'/components/Autoload.php');

// Router
$router = new Router();
$router->run();