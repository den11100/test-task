<?php

class User
{
    /**
     * @param string $admin
     */
    public static function auth($admin)
    {
        $_SESSION['admin'] = $admin;
    }

    /**
     * @return bool
     */
    public static function isGuest()
    {
        if (isset($_SESSION['admin']) && $_SESSION['admin'] === 'admin') {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public static function isAdmin()
    {
        if (isset($_SESSION['admin']) && $_SESSION['admin'] === 'admin') {
            return true;
        }
        return false;
    }
}