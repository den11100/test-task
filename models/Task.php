<?php

class Task
{
    const STATUS_NEW = 0;
    const STATUS_DONE = 1;

    /**
     * @param array $options
     * @return bool
     */
    public static function createTask($options)
    {
        $db = Db::getConnection();
        
        $sql = 'INSERT INTO task (username, email, task_text) VALUES (:username, :email, :task_text)';
                
        $result = $db->prepare($sql);
        
        $result->bindParam(':username',$options['username'],PDO::PARAM_STR);
        $result->bindParam(':email',$options['email'],PDO::PARAM_STR);
        $result->bindParam(':task_text',$options['task_text'],PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * @param int $id
     * @param array $options
     * @return bool
     */
    public static function updateTask($id, $options)
    {
        $db = Db::getConnection();

        $sql = "UPDATE task SET "
                . "task_text = :task_text, "
                . "status = :status "
                . "WHERE id = :id";
        
        $result = $db->prepare($sql);
        $result->bindParam(':id',$id,PDO::PARAM_INT);
        $result->bindParam(':task_text',$options['task_text'],PDO::PARAM_STR);
        $result->bindParam(':status',$options['status'],PDO::PARAM_INT);
        
        return $result->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public static function getTaskList($page, $limit, $column, $sort)
    {
        $db = Db::getConnection();

        $start = ($page - 1) * $limit;

        $sql = "SELECT * FROM task ORDER BY $column $sort LIMIT :start, :limit";
        $result = $db->prepare($sql);

        $result->bindParam(':start',$start,PDO::PARAM_INT);
        $result->bindParam(':limit',$limit,PDO::PARAM_INT);

        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function getTask($id)
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM task WHERE id = :id";
        $result = $db->prepare($sql);

        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @return integer
     */
    public static function getTaskCount()
    {
        $db = Db::getConnection();

        $sql = "SELECT count(id) as count FROM task";
        $result = $db->prepare($sql);

        $result->execute();
        return intval($result->fetchColumn());
    }

    /**
     * @param string $column
     * @return string
     */
    public static function validate($column)
    {
        if ($column === 'id' || $column === 'username' || $column === 'email' || $column = 'status') {
            return $column;
        }
        return 'id';
    }

    public static function validateFromInjections($string)
    {
        $string = str_replace("'", "", $string);
        $string = str_replace("\"", "", $string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        $string = addslashes($string);

        $quotes = array ("\x27", "\x22", "\x60", "\t", "\n", "\r", "*", "%", "<", ">", "?", "!" , "«", "»");
        $string = str_replace( $quotes, '', $string );

        return $string;
    }


}
