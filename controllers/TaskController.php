<?php


class TaskController
{
    public function actionCreate()
    {
        if (isset($_POST['submit'])) {

            $options['username'] = htmlspecialchars($_POST['username']);
            $options['email'] = htmlspecialchars($_POST['email']);
            $options['task_text'] = htmlspecialchars($_POST['task_text']);

            $errors = false;

            if (!isset($options['username']) || empty($options['username'])) {
                $errors[] = 'Заполните Имя';
            }

            if (!isset($options['email']) || empty($options['email'])) {
                $errors[] = 'Заполните email';
            }

            if (!isset($options['task_text']) || empty($options['task_text'])) {
                $errors[] = 'Добавьте текст задачи';
            }

            if ($errors === false) {
                Task::createTask($options);
                header("Location: /");
            }
        }

        require_once(ROOT . '/views/task/create.php');
        return true;
    }

    public function actionUpdate($id)
    {
        if (User::isGuest()) {
            header("Location: /");
        }

        $task = Task::getTask($id);

        if (isset($_POST['submit'])) {

            $options['task_text'] = htmlspecialchars($_POST['task_text']);

            if (isset($_POST['status']) && $_POST['status'] === "on") {
                $options['status'] = Task::STATUS_DONE;
            } else {
                $options['status'] = Task::STATUS_NEW;
            }

            $errors = false;

            if (!isset($options['task_text']) || empty($options['task_text'])) {
                $errors[] = 'Добавьте текст задачи';
            }

            if ($errors === false) {
                Task::updateTask($id, $options);
                header("Location: /");
            }
        }

        require_once(ROOT . '/views/task/update.php');
        return true;
    }
}
