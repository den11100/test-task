<?php


class SiteController
{
    public function actionIndex($page = 1, $column = "id", $sort = "asc")
    {
        if ($page == '') {
            $page = 1;
        }

        $column = Task::validateFromInjections($column);
        $sort = Task::validateFromInjections($sort);
        $page = Task::validateFromInjections($page);

        $sort = ($sort === "desc") ? "desc" : "asc";

        $column = Task::validate($column);

        $limit = 3;

        $taskCount = Task::getTaskCount();
        $pageCount = ceil($taskCount / $limit);

        $tasksList = Task::getTaskList($page, $limit, $column, $sort);

        require_once(ROOT . '/views/site/index.php');        
        return true;
    }

    public function actionLogin()
    {
        if (isset($_POST['submit'])) {

            $username = htmlspecialchars($_POST['username']);
            $password = htmlspecialchars($_POST['password']);

            $errors = false;

            if (!isset($username) || empty($username)) {
                $errors[] = 'Логин пустой';
            }
            if (!isset($password) || empty($password)) {
                $errors[] = 'Пароль пустой';
            }

            if ($username !== "admin") {
                $errors[] = 'Неверный логин';
            }
            if ($password !== "123") {
                $errors[] = 'Неверный пароль';
            }

            if ($errors === false) {
                User::auth('admin');
                header("Location: /");
            }
        }

        require_once(ROOT . '/views/site/login.php');
        return true;
    }

    public function actionLogout()
    {
        if (isset($_SESSION['admin'])) {
            unset($_SESSION['admin']);
            header("Location: /");
        }
        header("Location: /");
    }
}
