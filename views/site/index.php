<?php

include ROOT . '/views/layouts/header.php';

/* @var array $tasksList */
/* @var int $pageCount */

?>
        
<div class="container"">

    <div class="row">
        <div class="col-md-12">
            <p><a href="/task/create/" class="btn btn-success">Добавить задачу</a></p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php require_once("_taskList.php") ?>
        </div>
    </div>

</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>