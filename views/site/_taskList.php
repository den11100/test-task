<?php

/* @var array $tasksList */
/* @var int $pageCount */

?>

<table class="table table-hover table-striped">
    <thead>
    <tr>
        <th>#</th>

        <th style="min-width: 80px;">
            <a href="<?php echo "/page-$page/username/"?><?php echo ($sort === 'desc') ? 'asc' : 'desc' ?>">Имя</a>
            <?php if ($sort === 'desc' && $column === 'username'): ?>
                <span class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            <?php elseif($sort === 'asc' && $column === 'username'): ?>
                <span class="glyphicon glyphicon-sort-by-alphabet"></span>
            <?php endif; ?>
        </th>

        <th>
            <a href="<?php echo "/page-$page/email/"?><?php echo ($sort === 'desc') ? 'asc' : 'desc' ?>">Email</a>
            <?php if ($sort === 'desc' && $column === 'email'): ?>
                <span class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            <?php elseif($sort === 'asc' && $column === 'email'): ?>
                <span class="glyphicon glyphicon-sort-by-alphabet"></span>
            <?php endif; ?>
        </th>

        <th>Задача</th>

        <th>
            <a href="<?php echo "/page-$page/status/"?><?php echo ($sort === 'desc') ? 'asc' : 'desc' ?>">Статус</a>
            <?php if ($sort === 'desc' && $column === 'status'): ?>
                <span class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            <?php elseif($sort === 'asc' && $column === 'status'): ?>
                <span class="glyphicon glyphicon-sort-by-alphabet"></span>
            <?php endif; ?>
        </th>

        <?php if (User::isAdmin()): ?>
            <th> </th>
        <?php endif; ?>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($tasksList as $item): ?>
            <tr>
                <td><?php echo $item['id']; ?></td>
                <td><?php echo $item['username']; ?></td>
                <td><?php echo $item['email']; ?></td>
                <td><?php echo $item['task_text']; ?></td>
                <td><?php echo ($item['status'] == 0) ? "Новая" : "Выполнена"; ?></td>

                <?php if (User::isAdmin()): ?>
                    <td><a href="/task/update/<?php echo $item['id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<ul class="pagination">
    <?php for($i = 1; $i <= $pageCount; $i++): ?>
        <li class="<?php echo ($i == $page) ? "active": '' ?>">
            <a href="/page-<?php echo "$i/$column/$sort"?>"><?php echo $i?></a>
        </li>
    <?php endfor; ?>
</ul>
