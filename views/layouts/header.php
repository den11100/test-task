<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <meta name="keywords" content="" />
        <meta name="description" content="" />       
        <title>Задачи</title>
        <link href="/template/css/style.css" rel="stylesheet" type="text/css" media="screen" />
        <!-- Bootstrap -->
        <link href="/template/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    
<body>
    <!-- Static navbar -->
    <div class="navbar navbar-default navbar-static-top up-menu" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/">Главная</a></li>
                    <?php if (User::isAdmin()): ?>
                        <li><a href="/site/logout">Выход</a></li>
                    <?php else: ?>
                        <li><a href="/site/login">Вход</a></li>
                    <?php endif; ?>
                </ul>
            </div><!--/.nav-collapse -->
    
        </div>
    </div>
<div class="clearfix"></div>
	<!-- end #header -->

