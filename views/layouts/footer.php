<br /><br />
<div class="clearfix"></div>
<div id="footer">
      <div class="container">
        <p>Copyright (c) <?php echo date("Y") ?></p>
      </div>
</div>
<!-- end #footer -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/template/js/jquery-3.1.0.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/template/js/bootstrap.min.js"></script>
 
</body>
</html>