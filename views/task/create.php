<?php include ROOT. '/views/layouts/header.php'; ?>

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h4>Добавить новую задачу</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php if (isset($errors) && is_array($errors)): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li><?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>

            <form class="form" action="#" role="form" method="post">
                <div class="form-group">
                    <label for="username">Имя</label>
                    <input type="text" name="username" class="form-control" id="username" required value="<?php echo (isset($options['username'])) ? $options['username'] : "" ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" required value="<?php echo (isset($options['email'])) ? $options['email'] : "" ?>">
                </div>
                <div class="form-group">
                    <label for="taskText">Текст задачи</label>
                    <textarea class="form-control" id="taskText" rows="10" name="task_text" required><?php echo (isset($options['task_text'])) ? $options['task_text'] : ""?></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-primary">Создать</button>
                </div>
            </form>

        </div>
    </div>

</div>

<br/><br/>
<!-- end #page -->
<?php include ROOT. '/views/layouts/footer.php'; ?>




