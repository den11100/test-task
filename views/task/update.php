<?php include ROOT. '/views/layouts/header.php'; ?>

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h4>Изменить задачу #<?php echo (isset($task['id']) ? $task['id'] : '') ?></h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php if (isset($errors) && is_array($errors)): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li><?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>

            <?php if ($task): ?>
                <form class="form" action="#" role="form" method="post">
                    <div class="form-group">
                        <p>Имя: <?php echo $task['username'] ?></p>
                        <p>Email: <?php echo $task['email'] ?></p>
                    </div>
                    <div class="form-group">
                        <label for="taskText">Текст задачи</label>
                        <textarea class="form-control" id="taskText" rows="10" name="task_text" required><?php echo htmlspecialchars($task['task_text']) ?></textarea>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input name="status" type="checkbox" <?php echo ($task['status'] == 1) ? "checked" : '' ?>> Выполнена
                        </label>
                    </div>

                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary">Изменить</button>
                    </div>
                </form>
            <?php else: ?>
                <p>Задачи с таким id нет</p>
            <?php endif; ?>

        </div>
    </div>

</div>

<br/><br/>
<!-- end #page -->
<?php include ROOT. '/views/layouts/footer.php'; ?>




